<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contact;

use Validator, Input, Redirect; 

use Mail;

class ContactController extends Controller
{ 

	public function addContact(Request $request)
	{

		$rules = array(
			
			'email' => 'email|required',
		// $rules = array(
		// 	'event_type' => '|required',
		// 	'name' => 'required',
		// 	'email' => 'email|required',
		// 	'place' => 'required',
		// 	'city' => 'required',
		// 	'state' => 'required',
		// 	'phone' => 'required',
		// 	'date' => 'required',
		// 	'hour' => 'required',
		
		
		);

		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()){

			$Contacts = New Contact;
			$Contacts->event_type = $request->event_type;
			$Contacts->name = $request->name;
			$Contacts->place = $request->place;
			$Contacts->city = $request->city;
			$Contacts->state = $request->state;
			$Contacts->phone = $request->phone;
			$Contacts->date = $request->date;
			$Contacts->hour = $request->hour;
			$Contacts->email = $request->email;
			// $Contacts->status_id = 1;
			}
			if($Contacts->save()){
		
		    if ($validator->passes()){
			$status = true;

			Mail::to('fernando.santosrod@gmail.com')->send(new \App\Mail\contact());

			return view('index',compact('status'));
		}

		$status = false;
		return view('index',compact('status'));

		}
	}
}