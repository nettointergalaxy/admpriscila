<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'id', 'identity', 'name', 'event_type', 'email', 'place', 'city', 'state', 'phone', 'date', 'hour',
    ];
    
}
