<!DOCTYPE html>
<html lang="pt-BR" class="no-js">

    <head>
        <title>Priscilla Alcântara</title>
        
        <!-- Meta Data -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Coming Soon - Priscilla Alcântara © 2019">
        <meta name="keywords" content="hotsite, coming soon, priscilla, priscilla alcantara, alcântara, prikota, asu, 2019, convide, convidar, convidar a priscilla, evento, lançamento, em breve">
        <meta name="theme-color" content="#ccc">
        
        <!-- CSS Global Compulsory -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/style.css">
        
        <!-- CSS Implementing Plugins -->

        <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="assets/css/ionicons.min.css" type="text/css">
        <link rel="stylesheet" href="assets/css/animate.min.css" type="text/css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css" type="text/css">
        <link rel="stylesheet" href="assets/css/vegas.min.css" type="text/css">
        <link rel="stylesheet" href="assets/css/photoswipe/photoswipe.css"> 
        <link rel="stylesheet" href="assets/css/photoswipe/default-skin/default-skin.css"> 

        <link rel="stylesheet" href="assets/css/custom.css">
                
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <script src="assets/js/respond.min.js"></script>
        <![endif]-->
        
        <!--[if lt IE 11]>
            <link rel="stylesheet" type="text/css" href="/css/ie.css">
        <![endif]-->
        
        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i%7CRoboto:100,300,400,500,700,900' rel='stylesheet' type='text/css'>
        
        <!-- JS -->
        <script type="text/javascript" src="assets/js/modernizr.js"></script>
        
        <!-- Faviconss -->
        <link rel="shortcut icon" href="assets/images/favicon.png">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130761816-1"></script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-130761816-1');
        </script>

    </head>
    <body class="slideshow-background">
    
        <div id="preloader">
            <div id="loading-animation"></div>
        </div>
        
        <div class="main-container">
        
            <!-- Main Header -->
            <header class="main-header">
                <div class="header-block">
                    
                    <a class="header-logo" href="/">
                        <img src="assets/images/logo.png" alt="logo" class="logo-light">
                        <img src="assets/images/logo.png" alt="logo" class="logo-dark">
                    </a>
                </div>
            </header>


            <!-- HOME -->
            <div class="home-side">
            
                <!-- HOME CONTENT -->
                <section id="home" class="section fullscreen-element sm-pt-100 sm-pb-100">
                    <div class="overlay">
                        <div class="overlay-wrapper">
                            <div class="overlay-inner background-dark-5 opacity-40"></div>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-content">
                            <div class="container-fluid">
                                <span style="color: #fff;margin-left:15px;font-size:1.1em;"><strong>Convide a Pri</strong> para o seu evento!</span>
                                
                                <!-- FORMULÁRIO -->
                                <div class="row align-items-center">
                                    <div class="col-sm-5 pb-20 form-pri-bg">
                                        <div class="animated onstart" data-animation="fadeInUp" data-animation-delay="300" data-effect="fade" data-pagination="false" data-directionnav="false">

                                                <form id="cf" name="cf" action="/send-mail" method="post">
                                                    <div class="form-process"></div>
                                                    <div class="col-sm-12">
                                                            Tipo do Evento: <input type="text" title="Descreva o seu evento em 1 palavra" id="cf-event" name="event_type" placeholder="" class="form-control required"/>
                                                    </div>
                                                    <div class="col-sm-6">
                                                            Nome: <input type="text" id="cf-name" name="name" placeholder="" class="form-control required"/>
                                                    </div>
                                                    <div class="col-sm-6">
                                                            E-mail: <input type="email" id="cf-email" name="email" placeholder="" class="form-control required"/>
                                                    </div>
                                                    <div class="col-sm-4">
                                                            Local: <input type="text" id="cf-place" name="place" placeholder="" class="form-control required"/>
                                                    </div>
                                                    <div class="col-sm-4">
                                                            Cidade: <input type="text" id="cf-city" name="city" placeholder="" class="form-control required"/>
                                                    </div>
                                                    <div class="col-sm-4">
                                                            Estado: <input type="text" id="cf-state" name="state" placeholder="" class="form-control required"/>
                                                    </div>
                                                    <div class="col-sm-4">
                                                            Tel. / Cel.: <input type="text" id="cf-phone" name="phone" placeholder="" class="form-control required"/>
                                                    </div>
                                                    <div class="col-sm-4">
                                                            Data: <input type="text" id="cf-date" name="date" placeholder="dd/mm/aa" class="form-control required"/>
                                                    </div>
                                                    <div class="col-sm-4">
                                                            Hora: <input type="text" id="cf-time" name="hour" placeholder="" class="form-control required"/>
                                                    </div>
                                                    <div class="col-md-12 hidden">
                                                        <input type="text" id="cf-botcheck" name="cf-botcheck" value="" class="form-control"/>
                                                    </div>
                                                    <div class="col-md-6 mt-20">
                                                        <button class="btn btn-sm btn-block btn-purple" type="submit" id="cf-submit" name="cf-submit" title="Enviar"><strong>Enviar</strong></button>
                                                    </div>

                                                    <form id="cf" name="cf" action="/send-mail" method="post">
                                                    {{ csrf_field() }}
                                                    <div class="col-md-6 mt-20">
                                                        <a href="#eventos" class="btn btn-sm btn-block btn-white load-content" title="Próximos Eventos"><i class="fa fa-calendar mr-10" aria-hidden="true"></i> <strong>Agenda de Eventos</strong></a>
                                                    </div>
                                                </form>
                                            
                                            <div class="contact-form-result text-center pt-10"></div>
                                        </div>
                                    </div>
                                </div>
                                <form id="cf" name="cf" action="/send-mail" method="post">
                                {{ csrf_field() }}


                                <!-- EQUIPE / ACESSORIA -->
                                <div class="row align-items-center">
                                    <div class="col-sm-5 animated onstart equipe-pri-bg mt-20 pt-10 pb-10" data-animation="fadeInUp" data-animation-delay="300" data-effect="fade" data-pagination="false" data-directionnav="false">
                                        <div class="col-sm-6 info-producao">
                                            <p class="nopadding nomargin lead"><strong>Produção</strong></p>
                                            <p class="nopadding nomargin">Beto Fonseca</p>
                                            <p class="nopadding nomargin">Tel.: 11 953182728</p>
                                            <!-- <p class="nopadding nomargin">CX. POSTAL 76282</p>
                                            <p class="nopadding nomargin">CEP 06716-970</p> -->
                                            <a href="mailto:prikota@prikota.com.br"><p class="nopadding nomargin">prikota@prikota.com.br</p></a>
                                        </div>
                                        <div class="col-sm-6 info-acessoria">
                                            <p class="nopadding nomargin lead"><strong>Assessoria</strong></p>
                                            <p class="nopadding nomargin">Guilherme Oliveira</p>
                                            <p class="nopadding nomargin">Tel.: 11 988791840</p>
                                            <a href="mailto:acessoria@prikota.com.br"><p class="nopadding nomargin">assessoria@prikota.com.br</p></a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </div>

            <!-- Right Side -->
            <div class="right-side" id="eventos">
                <div class="container-fluid mt-20">
                    <div class="row">
                        <div class="col-md-2 pt-10">
                            <a href="#home" class="btn btn-xs btn-white load-content" id="botao-sidebar" title="Voltar"><strong><i class="fa fa-angle-left"></i> Voltar</strong></a>
                        </div>
                        <div class="col-md-10 pt-10">
                            <h3 class="text-white">Próximos Eventos</h3>
                        </div>
                    </div>
                </div>

                    <!-- Section - Eventos -->
                <section class="section pb-60">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="lista_eventos">
                                      <tr>
                                        <th>DATA</th>
                                        <th>CIDADE</th>
                                        <th>NOME / LOCAL</th>
                                      </tr>
                                      <tr>
                                        <td><a href="#">26 de JANEIRO - 2019</a></td>
                                        <td><a href="#">Brusque - SC</a></td>
                                        <td><a href="#">Show com Priscilla Alcântara - Óphera Casa de Eventos</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">02 de FEVEREIRO - 2019</a></td>
                                        <td><a href="#">Seropédica - RJ</a></td>
                                        <td><a href="#">Canta Jovem - Clube Casa Verde</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">09 de FEVEREIRO - 2019</a></td>
                                        <td><a href="#">Campinas - SP</a></td>
                                        <td><a href="#">Bola de Neve Campinas</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">16 de FEVEREIRO - 2019</a></td>
                                        <td><a href="#">Itapipoca - CE</a></td>
                                        <td><a href="#">Ginásio Esportivo Danusão</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">23 de FEVEREIRO - 2019</a></td>
                                        <td><a href="#">Orlando - Flórida</a></td>
                                        <td><a href="#">The Senda</a></td>
                                      </tr>
                                </table>
                            </div>
                        </div>
                    </div>          
                </section>

            </div>
            
            <!-- Footer -->
            <footer class="site-footer">
                <div class="container-fluid ml-30">
                    <p class="copyright" style="font-size:0.85em;"><strong>© 2019</strong> - Todos os direitos reservados.</p>
                    <nav class="socials-icons">
                        <ul>
                            <li><a href="https://www.facebook.com/priscillaalcantaraoficial/" target="_blank" class="social-icon"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/PriAlcantara" target="_blank" class="social-icon"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/priscillaalcantara/" target="_blank" class="social-icon"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </nav>
                </div>
            </footer>
        
        </div>

    

        <!-- JS -->

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>

        @if(isset($status) && $status)
        <script type="text/javascript">
            swal("Sucesso!", "Email de contato enviado com sucesso.", "success");
        </script>
        @endif

        @if(isset($status) && !$status)
        <script type="text/javascript">
            swal("Ops!", "Não foi possível enviar o email de contato, por favor tente novamente", "error");
        </script>
        @endif

    </body>
</html>